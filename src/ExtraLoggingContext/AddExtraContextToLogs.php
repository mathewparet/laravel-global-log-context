<?php
namespace mathewparet\LaravelGlobalLogContext\ExtraLoggingContext;

use Illuminate\Log\Logger;
use Illuminate\Http\Request;
use mathewparet\LaravelGlobalLogContext\Contracts\ContextDefinition;

class AddExtraContextToLogs
{
    public function __construct(protected Request $request, private ContextDefinition $contextDefinition) {}

    public function __invoke(Logger $logger)
    {
        $logger->withContext([
            'context' => $this->contextDefinition::context()
        ]);
    }
}