<?php
namespace mathewparet\LaravelGlobalLogContext\Contracts;

interface ContextDefinition
{
    public static function context(): array;
}